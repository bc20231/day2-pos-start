package org.example;

public class Item {
    public String barcode;
    public String name;
    public float quantity;
    public String unit;
    public float unitPrice;
    public float subtotal;

    public Item (String barcode, String name, float quantity, String unit, float unitPrice, float subtotal) {
        this.barcode = barcode;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.unitPrice = unitPrice;
        this.subtotal = subtotal;
    }
}
