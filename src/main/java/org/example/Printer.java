package org.example;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Printer {
    public String print(String[] cart, String[] promotions) {
        Map<String, Float> itemCounts = countItem(cart);
        Map<String, String> allItems = PosDataLoader.loadAllItems();
        List<String> promotionItems = Arrays.asList(promotions);
        String receipt = "***<No Profit Store> Shopping List***\n";
        receipt += printItems(itemCounts, allItems);
        receipt += printPromotionItems(itemCounts, allItems, promotionItems);
        receipt += printTotal(itemCounts, allItems, promotionItems);
        return receipt;
    }

    private LinkedHashMap<String, Float> countItem(String[] cartItems) {
        LinkedHashMap<String, Float> itemCounts = new LinkedHashMap<>();
        Arrays.stream(cartItems).forEach(item -> {
            String[] parts = item.split("-");
            String barcode = parts[0];
            float count = parts.length > 1 ? Float.parseFloat(parts[1]) : 1;
            itemCounts.put(barcode, itemCounts.getOrDefault(barcode, 0f) + count);
        });
        return itemCounts;
    }

    private List<Item> getSubtotalItems(Map<String, Float> itemCounts, Map<String, String> allItems) {
        List<Item> itemList = new ArrayList<>();
        itemCounts.forEach((barcode, quantity) -> {
            String itemDetails = allItems.get(barcode);
            String[] detailsParts = itemDetails.split(",");
            String name = detailsParts[0];
            float unitPrice = Float.parseFloat(detailsParts[1]);
            String unit = detailsParts[2];
            itemList.add(new Item(barcode, name, quantity, unit, unitPrice, quantity * unitPrice));
        });
        return itemList;
    }

    private String formatQuantity(float quantity) {
        DecimalFormat quantityFormat = new DecimalFormat("0.##");
        return quantityFormat.format(quantity);
    }

    private String formatPrice(float price) {
        DecimalFormat priceFormat = new DecimalFormat("0.00");
        return priceFormat.format(price);
    };

    private String printItems(Map<String, Float> itemCounts, Map<String, String> allItems) {
        List<Item> itemList = getSubtotalItems(itemCounts, allItems);
        String itemsSection = "----------------------\n";

        itemsSection += itemList.stream()
                .map(item -> {
                    String quantity = formatQuantity(item.quantity);
                    String unit = item.quantity > 1 ? item.unit + "s" : item.unit;
                    String unitPrice = formatPrice(item.unitPrice);
                    String subtotal = formatPrice(item.subtotal);
                    return String.format("Name：%s，Quantity：%s %s，Unit Price：%s(CNY)，Subtotal：%s(CNY)\n",
                            item.name, quantity, unit, unitPrice, subtotal);
                })
                .collect(Collectors.joining());
        return itemsSection;
    }

    private List<Item> getPromotionItems(Map<String, Float> itemCounts, Map<String, String> allItems, List<String> promotionItems) {
        List<Item> promotionItemList = new ArrayList<>();
        for (Map.Entry<String, Float> entry : itemCounts.entrySet()) {
            String barcode = entry.getKey();
            Float quantity = entry.getValue();
            float discountQuantity = (float) (int) (quantity / 3);
            if (promotionItems.contains(barcode) && discountQuantity > 0){
                String itemDetails = allItems.get(barcode);
                String[] detailsParts = itemDetails.split(",");
                String name = detailsParts[0];
                float unitPrice = Float.parseFloat(detailsParts[1]);
                String unit = detailsParts[2];
                promotionItemList.add(new Item(barcode, name, discountQuantity, unit, unitPrice, discountQuantity * unitPrice));
            }
        }
        return promotionItemList;
    }

    private String printPromotionItems(Map<String, Float> itemCounts, Map<String, String> allItems, List<String> promotionItems) {
        List<Item> itemList = getPromotionItems(itemCounts, allItems, promotionItems);
        String promotionSection = "----------------------\n" +
                "Buy two get one free items：\n";

        promotionSection += itemList.stream()
                .map(item -> {
                    String quantity = formatQuantity(item.quantity);
                    String unit = item.quantity > 1 ? item.unit + "s" : item.unit;
                    String subtotal = formatPrice(item.subtotal);
                    return String.format("Name：%s，Quantity：%s %s，Value：%s(CNY)\n",
                            item.name, quantity, unit, subtotal);
                })
                .collect(Collectors.joining());
        return promotionSection;
    }

    private float calculateTotal(Map<String, Float> itemCounts, Map<String, String> allItems) {
        float total;
        total = (float) itemCounts.entrySet().stream()
                .mapToDouble(entry -> {
                    String barcode = entry.getKey();
                    Float quantity = entry.getValue();
                    String itemDetails = allItems.get(barcode);
                    String[] detailsParts = itemDetails.split(",");
                    float unitPrice = Float.parseFloat(detailsParts[1]);
                    return quantity * unitPrice;
                })
                .sum();
        return total;
    }

    private float calculateSavings(Map<String, Float> itemCounts, Map<String, String> allItems, List<String> promotionItems) {
        float saved = 0;
        for (Map.Entry<String, Float> entry : itemCounts.entrySet()) {
            String barcode = entry.getKey();
            Float quantity = entry.getValue();
            float discountQuantity = (float) (int) (quantity / 3);
            if (promotionItems.contains(barcode)){
                String itemDetails = allItems.get(barcode);
                String[] detailsParts = itemDetails.split(",");
                float unitPrice = Float.parseFloat(detailsParts[1]);
                saved += (discountQuantity * unitPrice);
            }
        }
        return saved;
    }

    private float[] getTotalAndSavings(Map<String, Float> itemCounts, Map<String, String> allItems, List<String> promotionItems) {
        float[] result = new float[2];
        result[0] = calculateTotal(itemCounts, allItems) - calculateSavings(itemCounts, allItems,  promotionItems);
        result[1] = calculateSavings(itemCounts, allItems,  promotionItems);
        return result;
    }

    private String printTotal(Map<String, Float> itemCounts, Map<String, String> allItems, List<String> promotionItems) {
        String totalSection = "----------------------\n";

        DecimalFormat priceFormat = new DecimalFormat("0.00");
        float[] result = getTotalAndSavings(itemCounts, allItems,  promotionItems);
        String total = priceFormat.format(result[0]);
        String saved = priceFormat.format(result[1]);
        totalSection += "Total：" + total + "(CNY)\n";
        totalSection += "Saved：" + saved + "(CNY)\n";

        totalSection += "**********************\n";
        return totalSection;
    }
}