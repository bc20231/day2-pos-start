package org.example;

public class Main {
    public static void main(String[] args) {
        String[] cart = PosDataLoader.loadCart();
        String[] promotions = PosDataLoader.loadPromotion();
        Printer p = new Printer();
        System.out.println(p.print(cart, promotions));
    }
}