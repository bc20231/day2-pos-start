When settling purchases in a store, a Point of Sale (POS) system is used. The POS system calculates and prints receipts based on the items in the customer's shopping cart.

Input (example):
```
[
    'ITEM000001',
    'ITEM000001',
    'ITEM000001',
    'ITEM000001',
    'ITEM000001',
    'ITEM000003-2',
    'ITEM000005',
    'ITEM000005',
    'ITEM000005'
]
```
For items like 'ITEM000003-2', the part before "-" is the standard barcode, and the part after "-" is the quantity, which may be a decimal. When buying items that require weighing, the weighing machine generates such tags (Tag), and the POS system is responsible for recognizing and generating receipts.

To print a list, we need to provide data to the printing system so that it can print a message like the following (example):
```
***<No Profit Store> Receipt***

----------------------

Name: Badminton, Quantity: 5 pieces, Unit Price: 1.00 (CNY), Subtotal: 5.00 (CNY)

Name: Coca-Cola, Quantity: 3 bottles, Unit Price: 3.00 (CNY), Subtotal: 9.00 (CNY)

Name: Apple, Quantity: 2 pounds, Unit Price: 5.50 (CNY), Subtotal: 11.00 (CNY)

----------------------

Total: 25.00 (CNY)

**********************
```

For cases where the quantity is a decimal, consider the following example:
```
[
    'ITEM000004',
    'ITEM000004',
    'ITEM000002-1.5',
    'ITEM000003-3.3',
]
```
Buying the 'ITEM000002' item, which is 1.5 pounds when weighed, generates the 'ITEM000002-1.5' label from the weighing machine.

To print a list, we need to provide data to the printing system so that it can print a message like the following (example):
```
***<No Profit Store> Receipt***

----------------------

Name: Banana, Quantity: 1.5 pounds, Unit Price: 2.50 (CNY), Subtotal: 3.75 (CNY)

Name: Apple, Quantity: 3.3 pounds, Unit Price: 5.50 (CNY), Subtotal: 18.15 (CNY)

Name: Sprite, Quantity: 2 bottles, Unit Price: 3.50 (CNY), Subtotal: 7.00 (CNY)

----------------------

Total: 28.90 (CNY)

**********************
```

The store is currently offering a "buy two, get one free" promotion on some items. The cashier can set which barcodes correspond to items eligible for this promotion.

满三减一

When items eligible for the "buy two, get one free" promotion are purchased, the printing system should print a message like the following (example):
```
***<No Profit Store> Shopping List***

Name: Apple, Quantity: 2 pounds, Unit Price: 5.50 (CNY), Subtotal: 11.00 (CNY)

Name: Coca-Cola, Quantity: 3 bottles, Unit Price: 3.00 (CNY), Subtotal: 9.00 (CNY)

Name: Badminton, Quantity: 5 pieces, Unit Price: 1.00 (CNY), Subtotal: 5.00 (CNY)

----------------------

Buy two get one free items:

Name: Coca-Cola, Quantity: 1 bottle, Value: 3.00 (CNY)

Name: Badminton, Quantity: 1 piece, Value: 1.00 (CNY)

----------------------

Total: 21.00 (CNY)

Saved: 4.00 (CNY)

**********************
```

In the PosDataLoader class, there are three methods providing the data needed by the system:

loadAllItems: Information about all items
loadCart: Items in the shopping cart
loadPromotion: Items with promotions
Please use the provided methods to complete the settlement and print the receipt.